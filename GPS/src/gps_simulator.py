import serial
import math
import time

class ConfigData:
	params_filename = './.params'
	latitude = -30.00000
	longitude = -70.00000
	valid = 1
	speed = 10
	course = 360
	n_sat = 6
	utc_time = 0
	utc_date = 0
	def __init__(self):
		self.update_utc()
		self.update_params()
		return
	def update_params(self):
		f = open(self.params_filename, 'r')
		Nlines = sum(1 for line in open(self.params_filename))
		for ii in range(1,Nlines+1):
			line = f.readline();
			line = line[0:(len(line)-1)]
			Values = line.split(':')
			if 'long'.lower() == Values[0].lower():
				self.longitude = float(Values[1])
			if 'lat'.lower() == Values[0].lower():
				self.latitude = float(Values[1])
			if 'speed'.lower() == Values[0].lower():
				self.speed = float(Values[1])	
			if 'nsat'.lower() == Values[0].lower():
				self.n_sat = float(Values[1])	
			if 'valid'.lower() == Values[0].lower():
				self.valid = float(Values[1])	

	def get_ns_str(self):
		ns_indicator = 'S' if (self.latitude < 0) else 'N'
		return ns_indicator
	def get_ew_str(self):
		ew_indicator = 'W' if (self.longitude < 0) else 'E'
		return ew_indicator		
	def get_valid_indicator(self):
		valid_indicator = 'A' if self.valid else 'V'
		return valid_indicator		
	def update_utc(self):
		utc_struct = time.gmtime()
		self.utc_time = "%02d%02d%02d" % (utc_struct.tm_hour, utc_struct.tm_min, utc_struct.tm_sec)
		self.utc_date = "%02d%02d%02d" % (utc_struct.tm_mday, utc_struct.tm_mon, utc_struct.tm_year % 100)
	def GetGPRMC(self):
		gpstr = "$GPRMC,%s,%s,%09.4f,%s,%010.4f,%s,%05.1f,000.0,%s,%s,02\r\n"
		parameters = (  self.utc_time + ".000", 
			        self.get_valid_indicator(), 
				abs(self.latitude)*100, 
				self.get_ns_str(), 
				abs(self.longitude)*100, 
				self.get_ew_str(), 
				self.speed, 
				self.utc_date, 
				self.get_valid_indicator())  
		res = gpstr % parameters
		return res
	def GetGPGGA(self):
		gpstr = "$GPGGA,%s,%09.4f,%s,%010.4f,%s,1,%02d,0.0,0.0,0000,02\r\n"
		parameters = (  self.utc_time + ".000", 
				abs(self.latitude) * 100, 
				self.get_ns_str(), 
				abs(self.longitude)*100, 
				self.get_ew_str(), 
				self.n_sat)  
		res = gpstr % parameters
		return res
	def GetDefaultStr(self):
		parameters = (  self.n_sat, 
				self.get_valid_indicator(), 
				self.get_ns_str(), 
				abs(self.latitude)*10000, 
				self.get_ew_str(), 
				abs(self.longitude)*10000, 
				self.speed, 
				self.course, 
				self.utc_time, 
				self.utc_date)
		res = "%02d,%s,%s,%08d,%s,%09d,%03d,%03d,%s,%s" % parameters
		return res

	def GetConfigString(self, *args):
		self.update_utc()
		if(len(args) > 0):
			gpstr = {'GPGGA' : self.GetGPGGA(),
			         'GPRMC' : self.GetGPRMC()
				}[args[0]]
			return gpstr
		else:
			return self.GetDefaultStr()		


config_str = ConfigData().GetConfigString()
print "Writing the following string: %s" % config_str

# open the appropriate serial port
ser = serial.Serial('/dev/tty.usbserial-A600eK6e', 9600, bytesize=serial.EIGHTBITS)
ser.timeout = 0.05
DEFAULT_BUFFER_SIZE = 128
ser.open()

while True:
	cfg_data = ConfigData()
	gpstr = str(cfg_data.GetConfigString('GPRMC'))
	print "Writing " + gpstr
	ser.write(gpstr)
	time.sleep(0.2)
	gpstr = str(cfg_data.GetConfigString('GPGGA'))
	print "Writing " + gpstr
	ser.write(gpstr)
	time.sleep(1)

ser.close()
