
'''
Configures the Venus638 GPS module

Created on Feb 23, 2012

@author: JLH
'''
import serial
import threading
import time

class Message:
    messageID = 0x00
    body = ""
    def __init__(self, messageID=0x00, body=""):
        self.messageID = messageID
        self.body = body
    def to_str(self):
        start_seq = "\xA0\xA1"
        payload_length_str = "%c%c" % (self.payload_length() & 0xFF00,
                                       self.payload_length() & 0x00FF)
        payload_str = "%s%c%s" % (payload_length_str,
                                    self.messageID,
                                    self.body)
                                      
        trail_str = "%c\x0D\x0A" % self.checksum()
        msg_str = "%s%s%s" % (start_seq, payload_str, trail_str)
        return msg_str
    def checksum(self):
        chk = self.messageID
        for c in self.body:
            chk = chk ^ ord(c)
        chk = chk & 0xFFFF
        return chk
    def payload_length(self):
        pl = 1 + len(self.body)
        return pl
    def total_length(self):
        l = 8 + len(self.body)
        return l
    def change_gps_baudrate(self, new_baud_rate):
        self.messageID = 0x05
        int_baud_rate = {4800  : 0,
                         9600  : 1,
                         19200 : 2,
                         38400 : 3,
                         57600 : 4,
                         115200: 5}[new_baud_rate]
        sram_and_flash = 1
        com_port = 0x00
        body_str = "%c%c%c" % (com_port, int_baud_rate, sram_and_flash)
        self.body = body_str
        return self
    def configure_NMEA(self, str_set, enable = True):
        self.messageID = 0x08
        possibilities = ['GGA', 'GSA', 'GSV', 'GLL', 'RMC', 'VTG', 'ZDA']
        
        en_number = 1 if not enable else 0
        en_array = [en_number for ii in range(0, len(possibilities))]
        if enable:
            print "Enabling",
        else:
            print "Disabling",
        print "the following:",
        for ii in range(0, len(possibilities)):
            if any(possibilities[ii] in s for s in str_set):
                en_array[ii] = int(not en_number)
                print " %s" % possibilities[ii],
        
        self.body =  "".join(["%c" % en_array[ii] for ii in range(0, len(en_array))])
        sram_and_flash = 1
        self.body = "%s%c" % (self.body, sram_and_flash)
        print ""
        return self
    def configure_WAAS(self, enable = True):
        self.messageID = 0x37
        sram_and_flash = 1
        self.body = "%c%c" % (int(enable), sram_and_flash)
        print ""
        return self            
    def configure_nav_mode(self, pedestrian_not_car = False):
        self.messageID = 0x3C
        sram_and_flash = 1
        self.body = "%c%c" % (int(pedestrian_not_car), sram_and_flash)
        print ""
        return self 

        
class SerialInterface:
    serial_port = None
    receive_timer = None
    listen_enable = True
    receive_interval = 0.1 # seconds
    def __init__(self,
                 port_name='/dev/tty.usbserial-A600eK6e',
                 baudrate=9600,
                 time_out=0.08):
        self.serial_port = serial.Serial(port_name,
                                         baudrate=baudrate,
                                         timeout=time_out)
        self.serial_port.open()
        print "Port %s opened" % port_name

        self.receive_timer = threading.Timer(self.receive_interval,
                                            self.check_input_buffer)
    
    def send_message(self, message):
        if isinstance(message, Message):
            msg_str = message.to_str()
        else:
            msg_str = message
        print "Writing %i bytes: %s" % (len(msg_str), self.char_to_decimal(msg_str))
        bytes_written = self.serial_port.write(msg_str)
        if bytes_written != message.total_length():
            print "Couldn't write total number of bytes (wrote %i, was %i)" % (bytes_written, message.total_length())
        else:
            print "Write OK"
    
    def char_to_decimal(self, input_str):
        return " ".join(["%02x" % ord(input_str[ii]) for ii in range(0, len(input_str))])
    
    def check_input_buffer(self):
        if not self.listen_enable:
            return
        try:
            read_bytes = self.serial_port.read(1024)
        except:
            read_bytes = None

        if read_bytes is not None:
            if len(read_bytes) > 0:
                print "Buffer (l = %i): %s" % (len(read_bytes), read_bytes)
        if self.listen_enable:
            self.receive_timer = threading.Timer(self.receive_interval,
                                                 self.check_input_buffer).start()


    def listen(self, true_or_false=True):
        self.listen_enable = true_or_false
        if self.listen_enable:
            if not self.receive_timer.isAlive():
                self.receive_timer.start()
                print "Receive timer has started"
        else:
            self.receive_timer.cancel()
            
si = SerialInterface(baudrate = 19200)   
si.listen()
 
si.send_message(Message().change_gps_baudrate(19200))
si.send_message(Message().configure_NMEA(['GSA', 'GSV', 'GLL', 'VTG', 'ZDA'], enable = False)) 
si.send_message(Message().configure_WAAS(enable = True)) 
si.send_message(Message().configure_nav_mode(pedestrian_not_car = True)) 

time.sleep(5)
si.listen(False)
si.serial_port.close()

print "Done"

