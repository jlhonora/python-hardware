
'''
Configures the WiFly RN131C WiFi module

Created on Feb 24, 2012

@author: JLH
'''
import serial
import threading
import time

class Message:
    messageID = 0x00
    body = ""
    def __init__(self, messageID=0x00, body=""):
        self.messageID = messageID
        self.body = body
    def to_str(self):
        msg_str = "%s" % (self.body)
        return msg_str
    def total_length(self):
        l = len(self.body)
        return l
    def enter_command_mode(self):
        self.body = "$$$"
        return self
    def exit_command_mode(self):
        self.body = "\"exit\"\r"
        return self
    def set_ssid(self, ssid_name = "Satelinx_MSG"):
        self.body = "set wlan ssid %s\r\n" % ssid_name
        return self
    def set_channel(self, channel = 0):
        self.body = "set wlan channel %i\r\n" % channel
        return self
    def set_dhcp(self, mode = 2):
        self.body = "set ip dhcp %i\r\n" % mode
        return self
    def set_ip_address(self, address = "192.168.1.1"):
        self.body = "set ip address %s\r\n" % address
        return self
    def set_ip_backup(self, address = "192.168.1.2"):
        self.body = "set ip address %s\r\n" % address
        return self
    def set_ip_netmask(self, address = "255.255.255.0"):
        self.body = "set ip address %s\r\n" % address
        return self
    def set_start_string(self, msg_str = "Satelinx Messenger started"):
        self.body = "set comm remote %s\r\n" % msg_str
        return self
    def save_settings(self):
        self.body = "save\r\n"
        return self
    def send_reboot(self):
        self.body = "reboot\r\n"
        return self
    def set(self, param, param2, arg):
        if isinstance(arg, int):
            arg = "%i" % arg
        self.body = "set %s %s %s\r\n" % (param, param2, arg)
        return self
    def send_str(self, msg_str):
        self.body = msg_str + "\r\n"
        return self
        
class SerialInterface:
    serial_port = None
    receive_timer = None
    listen_enable = True
    receive_interval = 0.1 # seconds
    pre_send_offset = 1
    post_send_offset = 1
    def __init__(self,
                 port_name='/dev/tty.usbserial-A600eK6e',
                 baudrate=9600,
                 time_out=0.08):
        self.serial_port = serial.Serial(port_name,
                                         baudrate=baudrate,
                                         timeout=time_out)
        self.serial_port.open()
        print "Port %s opened" % port_name

        self.receive_timer = threading.Timer(self.receive_interval,
                                            self.check_input_buffer)
    
    def send_message(self, message):
        if isinstance(message, Message):
            msg_str = message.to_str()
        else:
            msg_str = message
        print "Writing %i bytes: %s (%s)" % (len(msg_str), msg_str, self.char_to_decimal(msg_str))
        time.sleep(self.pre_send_offset)
        bytes_written = self.serial_port.write(msg_str)
        time.sleep(self.post_send_offset)
        if bytes_written != message.total_length():
            print "Couldn't write total number of bytes (wrote %i, was %i)" % (bytes_written, message.total_length())
        else:
            print "Write OK"
    
    def char_to_decimal(self, input_str):
        return " ".join(["%02x" % ord(input_str[ii]) for ii in range(0, len(input_str))])
    
    def check_input_buffer(self):
        if not self.listen_enable:
            return
        try:
            read_bytes = self.serial_port.read(1024)
        except:
            read_bytes = None

        if read_bytes is not None:
            if len(read_bytes) > 0:
                print "Buffer (l = %i): %s" % (len(read_bytes), read_bytes)
        if self.listen_enable:
            self.receive_timer = threading.Timer(self.receive_interval,
                                                 self.check_input_buffer).start()


    def listen(self, true_or_false=True):
        self.listen_enable = true_or_false
        if self.listen_enable:
            if not self.receive_timer.isAlive():
                self.receive_timer.start()
                print "Receive timer has started"
        else:
            self.receive_timer.cancel()
            
si = SerialInterface(baudrate = 9600)   
si.listen()
 
# Enter command mode
si.send_message(Message().enter_command_mode())

# Write commands
si.send_message(Message().set_ssid("Satelinx_MSG"))
si.send_message(Message().set_channel(channel = 1))
si.send_message(Message().set_ip_address())
si.send_message(Message().set_ip_backup())
si.send_message(Message().set_ip_netmask())
si.send_message(Message().set_start_string("Satelinx Messenger"))
si.send_message(Message().set_dhcp(mode = 2))
si.send_message(Message().set("wlan", "join", 4))
si.send_message(Message().send_str("set wlan passphrase satelinx123"))

# Save and reboot
si.send_message(Message().save_settings())
si.send_message(Message().send_str("reboot"))

# Exit routine
time.sleep(5)
si.listen(False)
si.serial_port.close()
print "Done"

