#!/usr/bin/env python
import serial
import time

# The following connections must be made:
#
# FTDI       Iridium
#---------------------
#
# RXI        RX (out)
# TXO        TX (in)
# 3.3        ON/OFF
# DTR (out)  DTR (in)
# CTS (in)   RTS (out)
# 
# Baudrate = 19200

def IridiumWrite(s, str):
	print "Writing ", str
	s.write(str)
	time.sleep(0.5)
	str_read = s.read(20)
	if(len(str_read) == 0):
		str_error = "Nothing received from serial port"
		print str_error
		raise Exception(str_error)
	else:
		print str_read

print "Starting Iridium Automatic configuration"

s = serial.Serial('/dev/tty.usbserial-A600eK6e', 
				baudrate     = 19200, 
				bytesize     = serial.EIGHTBITS, 
				parity       = serial.PARITY_NONE, 
				stopbits     = serial.STOPBITS_ONE, 
				timeout      = 0.5, 
				xonxoff      = False, 
				rtscts       = False, # Set to True for the first time...
				dsrdtr       = False, 
				interCharTimeout = None);

print "Serial port opened"

IridiumWrite(s, "AT\r\n");
IridiumWrite(s, "ATE0\r\n");
IridiumWrite(s, "AT&D0\r\n");
IridiumWrite(s, "AT&K0\r\n");
IridiumWrite(s, "AT*R1\r\n");
IridiumWrite(s, "AT+SBDMTA=1\r\n");
IridiumWrite(s, "AT&W0\r\n");
IridiumWrite(s, "AT&Y0\r\n");

print "Done"
s.close();
